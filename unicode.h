typedef unsigned long	UTF32;	/* at least 32 bits */
typedef unsigned char	UTF8;	/* 8 bits */
typedef unsigned char	Boolean; /* 0 or 1 */

#define UNI_MAX_UTF32 (UTF32)0x7FFFFFFF
#define UNI_REPLACEMENT_CHAR (UTF32)0x0000FFFD
